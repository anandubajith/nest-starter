import {
    Controller,
    Post,
    Body,
    Param,
    Delete,
    Put,
    UseGuards,
    Get,
    HttpException,
    HttpStatus,
  } from '@nestjs/common';
  import { UserService } from './user.service';
  import { User, UserRole } from './model/user.interface';
  import { from, Observable, of } from 'rxjs';
  import { catchError, map, tap } from 'rxjs/operators';
  import { hasRoles } from '../auth/decorators/roles.decorator';
  import { JwtAuthGuard } from '../auth/guards/jwt-guard';
  import { RolesGuard } from '../auth/guards/roles.guard';
  import { UserIsUserGuard } from '../auth/guards/UserIsUser.guard';
  import { CurrentUser } from 'src/auth/decorators/currentUser.decorator';
  
  @Controller('users')
  export class UserController {
    constructor(private userService: UserService) { }
  
    @Post('login')
    login(@Body() user: User): Observable<Object> {
      return this.userService.login(user).pipe(
        map((response: object) => {
          return response;
        }),
        catchError(_ => {
          throw new HttpException('Invalid login', HttpStatus.UNAUTHORIZED);
        }),
      );
    }
}